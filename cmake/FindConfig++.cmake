#  Equinox: SDR platform for realtime applications
#  Copyright (C) 2017  Manolis Surligas <surligas@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


###############################################################################
# CMake module for the libconfig++ library
# This module defines the variables:
#    CONFIG++_FOUND: True if the library found, undefined otherwise
#    CONFIG++_INCLUDE_DIR: The include directory
#    CONFIG++_LIBRARY: The libconfig++ library
###############################################################################

FIND_PATH(CONFIG++_INCLUDE_DIR libconfig.h++)

FIND_LIBRARY(CONFIG++_LIBRARY NAMES config++) 

if (CONFIG++_INCLUDE_DIR AND CONFIG++_LIBRARY)
    set(CONFIG++_FOUND TRUE)
endif ( CONFIG++_INCLUDE_DIR AND CONFIG++_LIBRARY)

if (CONFIG++_FOUND)
    if (NOT CONFIG++_FIND_QUIETLY)
    message(STATUS "Found Config++: ${CONFIG++_LIBRARY}")
    endif (NOT  CONFIG++_FIND_QUIETLY)
else(CONFIG++_FOUND)
    if (Config++_FIND_REQUIRED)
    if(NOT CONFIG++_INCLUDE_DIR)
        MESSAGE(FATAL_ERROR "Could not find LibConfig++ header file!")
    endif(NOT CONFIG++_INCLUDE_DIR)

    if(NOT CONFIG++_LIBRARY)
        MESSAGE(FATAL_ERROR "Could not find LibConfig++ library file!")
    endif(NOT CONFIG++_LIBRARY)
    endif (Config++_FIND_REQUIRED)
endif (CONFIG++_FOUND)