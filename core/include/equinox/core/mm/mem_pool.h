/*
 * Equinox: SDR platform for realtime applications
 * Copyright (C) 2017  Manolis Surligas <surligas@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef CORE_INCLUDE_EQUINOX_CORE_MM_MEM_POOL_H_
#define CORE_INCLUDE_EQUINOX_CORE_MM_MEM_POOL_H_

#include <equinox/core/core.h>
#include <equinox/msg/msg.h>
#include <equinox/core/mm/circ_buffer.h>

#include <deque>

namespace eqnx
{

namespace core
{

/**
 * @ingroup core
 *
 * This class provides a memory pool that tries to eliminate memory
 * allocation during runtime, in order to decrease the latency of the
 * messages that are exchanged between the processing kernels.
 *
 * Equinox tries to exploit the placement of the DSP kernels and provide
 * to them appropriate memory, trying to decrease the penalties imposed
 * by cache misses e.t.c.
 *
 * Each Equinox worker may have at least one memory pool. In most cases
 * a worker may have a memory pool for exchanging data with other workers
 * operating in different threads and a second memory pool suitable for
 * messages exchanged between DSP kernels inside the same worker.
 *
 * This is a virtual class and does not perform any memory allocation.
 * There are more specific memory pool classes that extend this class and
 * allocate memory for specific use.
 */
class mem_pool : public std::enable_shared_from_this<mem_pool>
{
public:
  typedef std::shared_ptr<mem_pool> sptr;

  typedef struct
  {
    size_t key;
    void *ptr;
  } mem_block_t;

  virtual
  ~mem_pool ();

  /* Do not allow copies of this class */
  mem_pool (const mem_pool&) = delete;

  /**
   * Creates a new message with a maximum size equal the size declared
   * during the construction of the memory pool
   * @return a shared pointer to the new message object
   */
  virtual msg::sptr
  new_msg () = 0;

  /**
   * Release the resources of a memory block and make them again available
   * to the memory pool.
   *
   * @param key the key of the memory block
   */
  virtual void
  release (size_t key);

  /**
   * @return true if the memory pool is full (no new message can be allocated).
   */
  bool
  full();

protected:
  mem_pool (size_t msg_num, size_t max_msg_size);

  const size_t d_blocks_num;
  const size_t d_block_size;

  mem_block_t *d_blocks;
  circ_buffer<mem_block_t *> d_free_blocks;
};

}  // namespace core

}  // namespace eqnx

#endif /* CORE_INCLUDE_EQUINOX_CORE_MM_MEM_POOL_H_ */
