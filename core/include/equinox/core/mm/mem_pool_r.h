/*
 * Equinox: SDR platform for realtime applications
 * Copyright (C) 2017  Manolis Surligas <surligas@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef CORE_INCLUDE_EQUINOX_CORE_MM_MEM_POOL_R_H_
#define CORE_INCLUDE_EQUINOX_CORE_MM_MEM_POOL_R_H_

#include <equinox/core/mm/mem_pool.h>

#include <condition_variable>
#include <mutex>

namespace eqnx
{

namespace core
{

/**
 * @ingroup core
 *
 * A reentrant memory pool suitable for processing kernels that exchange
 * messages on different processing threads.
 *
 * Normally the Equinox scheduler will peak the proper memory pool for
 * messages exchanged between different processing kernels, depending the
 * execution placement (same thread vs different threads)
 */
class mem_pool_r : virtual public mem_pool
{
public:
  static sptr
  make_shared (size_t msg_num, size_t max_msg_size);

  ~mem_pool_r ();

  msg::sptr
  new_msg ();

  void
  release (size_t key);
protected:
  mem_pool_r (size_t msg_num, size_t max_msg_size);

private:
  std::condition_variable d_cv;
  std::mutex d_mtx;
};

}  // namespace core

}  // namespace eqnx

#endif /* CORE_INCLUDE_EQUINOX_CORE_MM_MEM_POOL_R_H_ */
