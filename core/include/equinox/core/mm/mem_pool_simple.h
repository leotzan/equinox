/*
 * Equinox: SDR platform for realtime applications
 * Copyright (C) 2017  Manolis Surligas <surligas@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef CORE_INCLUDE_EQUINOX_CORE_MM_MEM_POOL_SIMPLE_H_
#define CORE_INCLUDE_EQUINOX_CORE_MM_MEM_POOL_SIMPLE_H_

#include <equinox/core/mm/mem_pool.h>

namespace eqnx
{

namespace core
{

/**
 * @ingroup core
 *
 * A simple memory pool suitable for processing kernels that exchange
 * messages on the same processing thread. Hence, it does not use any
 * parallel execution synchronization primitive. If messages derived from
 * this memory pool are exchanged between different threads the result is
 * totally undefined.
 *
 * Normally the Equinox scheduler will peak the proper memory pool for
 * messages exchanged between different processing kernels, depending the
 * execution placement (same thread vs different threads)
 */
class mem_pool_simple : public mem_pool
{
public:

  static sptr
  make_shared (size_t msg_num, size_t max_msg_size);

  ~mem_pool_simple ();

  msg::sptr
  new_msg ();

protected:
  mem_pool_simple (size_t msg_num, size_t max_msg_size);
};

}  // namespace core

}  // namespace eqnx

#endif /* CORE_INCLUDE_EQUINOX_CORE_MM_MEM_POOL_SIMPLE_H_ */
