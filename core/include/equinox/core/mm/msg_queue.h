/*
 * Equinox: SDR platform for realtime applications
 * Copyright (C) 2017  Manolis Surligas <surligas@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef CORE_INCLUDE_EQUINOX_MM_MSG_QUEUE_H_
#define CORE_INCLUDE_EQUINOX_MM_MSG_QUEUE_H_

#include <equinox/core/core.h>
#include <equinox/msg/msg.h>
#include <equinox/core/mm/circ_buffer.h>
#include <equinox/core/mm/msg_queue_consumer.h>

#include <chrono>
#include <mutex>

namespace eqnx
{

namespace core
{
/**
 * @ingroup core
 *
 * Message queue base class. The message queues of Equinox are a type of
 * single-producer, multiple consumers. Multiple consumers are
 * not competing on the elements of the message queue. Readers can consume
 * messages from the queue independently from other readers registered in
 * the same message queue.  However, in order a message to be considered
 * as read and its resources to be freed, all the registered consumers
 * should dequeue this particular element.
 *
 * With this scheme, a processing block can output exact copies of its data
 * to multiple processing blocks in an efficient and secure way.
 */
class msg_queue : public std::enable_shared_from_this<msg_queue>
{
public:
  /**
   * Shared pointer of the message queue. Note that a msg_queue object can
   * not be copied. The object can be shared or used by multiple entities
   * using only the shared pointer.
   */
  typedef std::shared_ptr<msg_queue> sptr;

  /**
   * Message queue error codes
   */
  typedef enum
  {
    EQNX_MSGQ_NO_ERROR = 0,      //!< No error occurred
    EQNX_MSGQ_TIMER_EXPIRED = -1, //!< the timed-wait timer expired and no action was performed
    EQNX_MSGQ_NO_MEM = -2,   //!< there is not enough space in the message queue
    EQNX_MSGQ_NO_DATA = -3,      //!< there is no data on the message queue
    EQNX_MSGQ_INVALID = -4, //!< invalid parameter or invalid state of the message queue
    EQNX_MSGQ_UNKNOWN_ERROR = -5 //!< unknown error
  } msgq_code_t;

  virtual
  ~msg_queue ();

  /* Do not allow copies of this class */
  msg_queue (const msg_queue&) = delete;

  /**
   * Push a new message on the message queue. This method may block waiting
   * for available space
   * @param msg the message to enqueue
   * @return appropriate #msgq_code_t code
   */
  virtual int
  push (msg::sptr msg) = 0;

  /**
   * Push a new message on the message queue. This method may block waiting
   * for available space for at least @timeou_us us. If the timeout expires
   * the method will return EQNX_MSGQ_TIMER_EXPIRED
   * @param msg
   * @param timeout_us
   * @return appropriate #msgq_code_t code
   */
  virtual int
  push_timedwait (msg::sptr msg, size_t timeout_us) = 0;

  /**
   *
   * @param msg
   * @return appropriate #msgq_code_t code
   */
  virtual int
  push_nowait (msg::sptr msg) = 0;

  /**
   *
   * @param reader
   * @return
   */
  virtual msg::sptr
  pop (size_t reader) = 0;

  /**
   *
   * @param reader
   * @param timeout_us
   * @return
   */
  virtual msg::sptr
  pop_timedwait (size_t reader, size_t timeout_us) = 0;

  /**
   *
   * @param reader
   * @return
   */
  virtual msg::sptr
  pop_nowait (size_t reader) = 0;

  /**
   * Clears all the messages currently on the message queue
   */
  virtual void
  clear () = 0;

  /**
   *
   * @return true if the queue is empty, false otherwise
   */
  virtual bool
  empty() = 0;

  /**
   *
   * @param reader the reader ID
   * @return true if the queue is empty for the specific reader, false otherwise
   */
  virtual bool
  empty (size_t reader) = 0;

  /**
   *
   * @return true if the queue is full, false otherwise
   */
  virtual bool
  full() = 0;

  size_t
  get_consumers_num ();

  const std::string
  name ();

  msg_queue_consumer::uptr
  register_consumer (const std::string& name);

  virtual void
  stop ();

protected:
  const std::string d_name;
  const size_t d_consumers_num;
  const bool d_coallescing;
  const bool d_profiling;
  bool d_stop;
  circ_buffer<msg::sptr> d_q;
  size_t d_consumer_idx;
  std::mutex d_cossumers_lock;

  sptr
  get_sptr ();

  msg_queue (const std::string& name, size_t size, size_t consumers = 1,
             bool allow_coalescing = false, bool profiling_mode = false);
};

}  // namespace core

}  // namespace eqnx

#endif /* CORE_INCLUDE_EQUINOX_MM_MSG_QUEUE_H_ */
