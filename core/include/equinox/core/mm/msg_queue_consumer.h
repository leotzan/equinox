/*
 * Equinox: SDR platform for realtime applications
 * Copyright (C) 2017  Manolis Surligas <surligas@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef CORE_INCLUDE_EQUINOX_CORE_MM_MSG_QUEUE_CONSUMER_H_
#define CORE_INCLUDE_EQUINOX_CORE_MM_MSG_QUEUE_CONSUMER_H_

#include <equinox/core/core.h>
#include <equinox/msg/msg.h>

#include <chrono>

namespace eqnx
{

namespace core
{

/**
 * @ingroup core
 *
 * This class provides a unified way to consume messages from a message
 * queue.
 */
class msg_queue_consumer
{
public:
  /**
   * For greater safety a message queue consumer cannot be passed around for
   * any reason. A message queue consumer instance should be used only by the
   * entity that created it.
   */
  typedef std::unique_ptr<msg_queue_consumer> uptr;

  static uptr
  make (const std::string& name, size_t id, std::shared_ptr<msg_queue> msgq);

  msg_queue_consumer (const msg_queue_consumer&) = delete;

  ~msg_queue_consumer ();

  msg::sptr
  read ();

  template<typename _Rep>
    msg::sptr
    read (const std::chrono::duration<_Rep>& timeout);

  const std::string
  name ();

  size_t
  id ();

  bool
  empty();

private:
  const std::string d_name;
  const size_t d_id;
  std::shared_ptr<msg_queue> d_msgq;

  msg_queue_consumer (const std::string& name, size_t id,
                      std::shared_ptr<msg_queue> msgq);
};

}  // namespace core

}  // namespace eqnx

#endif /* CORE_INCLUDE_EQUINOX_CORE_MM_MSG_QUEUE_CONSUMER_H_ */
