/*
 * Equinox: SDR platform for realtime applications
 * Copyright (C) 2017  Manolis Surligas <surligas@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef CORE_INCLUDE_EQUINOX_CORE_MM_MSG_QUEUE_EXCEPTION_H_
#define CORE_INCLUDE_EQUINOX_CORE_MM_MSG_QUEUE_EXCEPTION_H_

#include <exception>
#include <iostream>

namespace eqnx
{

namespace core
{
/**
 * @ingroup core
 *
 * Message queue exception class. When the program is finished either
 * by an internal signal or by the user, message queues of the processing
 * kernels should be informed in order to unblock from any blocking operation
 * waiting for resources.
 *
 * For a clear and bug-safe operation, Equinox uses this exception class
 * for such cases.
 */
class msg_queue_exception : public std::exception
{
public:
  /**
   *
   * @return a C-style character string describing the general cause
   * of the current error.
   */
  const char *
  what () const throw ()
  {
    return "Message queue stopped";
  }
};
}  // namespace core

}  // namespace eqnx

#endif /* CORE_INCLUDE_EQUINOX_CORE_MM_MSG_QUEUE_EXCEPTION_H_ */
