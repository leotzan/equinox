/*
 * Equinox: SDR platform for realtime applications
 * Copyright (C) 2017  Manolis Surligas <surligas@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef CORE_INCLUDE_EQUINOX_CORE_MM_MSG_QUEUE_R_H_
#define CORE_INCLUDE_EQUINOX_CORE_MM_MSG_QUEUE_R_H_

#include <equinox/core/mm/msg_queue.h>

#include <condition_variable>
#include <mutex>

namespace eqnx
{

namespace core
{

class msg_queue_r : public msg_queue
{
public:
  static sptr
  make_shared (const std::string& name, size_t size, size_t consumers = 1,
               bool allow_coalescing = false, bool profiling_mode = false);

  ~msg_queue_r ();

  int
  push (msg::sptr msg);

  int
  push_timedwait (msg::sptr msg, size_t timeout_us);

  int
  push_nowait (msg::sptr msg);

  msg::sptr
  pop (size_t reader);

  msg::sptr
  pop_timedwait (size_t reader, size_t timeout_us);

  msg::sptr
  pop_nowait (size_t reader);

  bool
  empty();

  bool
  empty(size_t reader);

  bool
  full();

  void
  clear ();

private:
  msg_queue_r (const std::string& name, size_t size, size_t consumers = 1,
                  bool allow_coalescing = false, bool profiling_mode = false);

  std::condition_variable d_cv;
  std::mutex d_mtx;

};

}  // namespace core

}  // namespace eqnx

#endif /* CORE_INCLUDE_EQUINOX_CORE_MM_MSG_QUEUE_R_H_ */
