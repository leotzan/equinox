/*
 * Equinox: SDR platform for realtime applications
 * Copyright (C) 2017  Manolis Surligas <surligas@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef CORE_INCLUDE_EQUINOX_CORE_SCHED_CONNECTION_ANCHOR_H_
#define CORE_INCLUDE_EQUINOX_CORE_SCHED_CONNECTION_ANCHOR_H_

#include <equinox/core/core.h>
#include <equinox/runtime/io_port.h>
#include <equinox/runtime/kernel.h>

namespace eqnx
{

namespace core
{

/**
 * @ingroup core
 *
 * A convenient wrapper describing an endpoint of a connection
 */
class connection_anchor
{
public:
  connection_anchor ();

  connection_anchor (std::shared_ptr<kernel> k, std::shared_ptr<io_port> port);

  std::shared_ptr<kernel>
  get_kernel () const;

  std::shared_ptr<io_port>
  get_port () const;

  friend std::shared_ptr<connection>
  operator >> (const connection_anchor& anchor0,
               const connection_anchor& anchor1);

private:
  std::shared_ptr<kernel> d_kernel;
  std::shared_ptr<io_port> d_port;
};

}  // namespace core

}  // namespace eqnx

#endif /* CORE_INCLUDE_EQUINOX_CORE_SCHED_CONNECTION_ANCHOR_H_ */
