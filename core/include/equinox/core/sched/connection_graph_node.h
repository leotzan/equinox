/*
 * Equinox: SDR platform for realtime applications
 * Copyright (C) 2017  Manolis Surligas <surligas@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef CORE_INCLUDE_EQUINOX_CORE_SCHED_CONNECTION_GRAPH_NODE_H_
#define CORE_INCLUDE_EQUINOX_CORE_SCHED_CONNECTION_GRAPH_NODE_H_

#include <equinox/runtime/kernel.h>
#include <equinox/core/sched/connection.h>

#include <map>
#include <deque>

namespace eqnx
{

namespace core
{
/**
 * @ingroup core
 *
 * Class describing a node of the connection graph. Each of the connection
 * graph node is actually the corresponding processing kernel among with the
 * connections to and from this kernel.
 */
class connection_graph_node
{
public:
  typedef std::shared_ptr<connection_graph_node> sptr;

  static sptr
  make_shared ();

  void
  add_input_connection (connection::sptr c);

  void
  add_output_connection (connection::sptr c);

  float
  weight ();

  void
  set_weight (float w);

  size_t
  worker_id ();

  void
  set_worker_id (size_t id);

  void
  set_kernel (kernel::sptr k);

  kernel::sptr
  get_kernel ();

  int
  depth ();

  void
  set_depth (int d);

  bool
  depth_valid ();

  void
  reset ();

  void
  set_dfs_visited (bool visited);

  bool
  dfs_visited ();

  size_t
  get_input_edges_num ();

  size_t
  get_input_edges_num (bool processed);

  size_t
  get_output_edges_num ();

  connection::sptr
  get_input_edge (size_t index);

  connection::sptr
  get_output_edge (size_t index);

  std::deque<connection::sptr>
  get_ouput_port_connections(const std::string& name);

  std::vector<std::string>
  get_output_ports();

private:
  connection_graph_node ();

  kernel::sptr d_kernel;
  bool d_kernel_set;
  std::deque<connection::sptr> d_inputs;
  std::deque<connection::sptr> d_outputs;
  std::map<std::string, std::deque<connection::sptr>>
                               d_outputs_map;

  /**
   * Set to true if this node has been visited from a DFS for extracting
   * the connected components of a graph
   */
  bool d_dfs_visited;

  /**
   * The depth of the node in the graph. As depth we considered the distance
   * from a root. As root source kernels are considered. Depth of the roor
   * is 0.
   */
  int d_depth;

  /**
   * The weight of the kernel that corresponds to this particular node.
   * The range of the weight can be (0.0 - 1.0] with smaller values
   * indicating a kernel with less processing requirements
   * whereas higher values correspond to more demanding kernels.
   *
   * By default each node is assigned with a weight of 1.
   */
  float d_weight;

  /**
   * The ID of the worker that this graph node runs on
   */
  size_t d_worker_id;
};

}  // namespace core

}  // namespace eqnx

#endif /* CORE_INCLUDE_EQUINOX_CORE_SCHED_CONNECTION_GRAPH_NODE_H_ */
