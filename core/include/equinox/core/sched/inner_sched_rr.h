/*
 * Equinox: SDR platform for realtime applications
 * Copyright (C) 2017  Manolis Surligas <surligas@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef CORE_INCLUDE_EQUINOX_CORE_SCHED_INNER_SCHED_RR_H_
#define CORE_INCLUDE_EQUINOX_CORE_SCHED_INNER_SCHED_RR_H_

#include <equinox/core/sched/inner_sched.h>

#include <list>
#include <mutex>
#include <condition_variable>

namespace eqnx
{

namespace core
{

/**
 * @ingroup sched
 *
 * Round-Robin inner scheduler. This inner scheduler executes the assigned
 * kernels in a round-robin fashion. If a kernel cannot be executed, it is
 * just skipped. If no kernels can be executed, the behavior relies on the
 * implementation of the message queues that connect edge kernels between
 * different worker threads.
 */
class inner_sched_rr : public virtual inner_sched
{
public:
  static inner_sched::sptr
  make_shared ();

  void
  start ();

  void
  stop ();

  void
  run ();

  void
  dry_run ();

  void
  insert_kernel (kernel_runnable::sptr kernel);

protected:
  inner_sched_rr ();
private:
  std::mutex d_mtx;
  std::condition_variable d_cond;
  std::list<kernel_runnable::sptr> d_kernels;

};

}  // namespace core

}  // namespace eqnx

#endif /* CORE_INCLUDE_EQUINOX_CORE_SCHED_INNER_SCHED_RR_H_ */
