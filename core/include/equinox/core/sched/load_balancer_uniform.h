/*
 * Equinox: SDR platform for realtime applications
 * Copyright (C) 2017  Manolis Surligas <surligas@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef CORE_INCLUDE_EQUINOX_CORE_SCHED_LOAD_BALANCER_UNIFORM_H_
#define CORE_INCLUDE_EQUINOX_CORE_SCHED_LOAD_BALANCER_UNIFORM_H_

#include <equinox/core/sched/load_balancer.h>

namespace eqnx
{

namespace core
{
class load_balancer_uniform : public virtual load_balancer
{
public:
  load_balancer_uniform (connection_graph::sptr graph, size_t workers_num = 1);

  bool
  analyze ();

  void
  reset ();

  std::deque<kernel::sptr>
  get_worker_assignement (size_t index);

  bool
  find_kernel(const size_t index, const kernel::sptr k);

private:
};
}  // namespace core

}  // namespace eqnx

#endif /* CORE_INCLUDE_EQUINOX_CORE_SCHED_LOAD_BALANCER_UNIFORM_H_ */
