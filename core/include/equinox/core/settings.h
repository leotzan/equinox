/*
 * Equinox: SDR platform for realtime applications
 * Copyright (C) 2017  Manolis Surligas <surligas@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef CORE_INCLUDE_EQUINOX_CORE_SETTINGS_H_
#define CORE_INCLUDE_EQUINOX_CORE_SETTINGS_H_

#include <memory>
#include <string>
#include <mutex>
#include <libconfig.h++>

namespace eqnx
{
namespace core
{
/**
 * @ingroup core
 *
 * The Equinox platform supports a set of configuration parameters that
 * can be specified by an appropriate configuration file.
 *
 * This class provides a mechanism that allows to retrieve this configuration
 * parameters, alter them and store them again to a configuration file.
 *
 * It allows also to load the default settings, in case no configuration file
 * is available.
 *
 */
class settings
{
public:

  typedef std::shared_ptr<settings> sptr;

  sptr
  make ();
  sptr
  make (const std::string& file);

  ~settings ();

  size_t
  get_workers_per_core ();
  size_t
  get_max_msg_size ();
  std::string
  config_file ();

  void
  set_workers_per_core (size_t n);
  void
  set_max_msg_size (size_t s);

  void
  store ();
  void
  store (const std::string& file);
private:

  settings (const std::string& file);
  settings ();

  /* Do not allow copies of this class */
  settings (const settings&) = delete;

  static const std::string workers_per_core_attr;
  static const std::string max_msg_size_attr;

  static const size_t default_workers_per_core = 1;
  static const size_t default_max_msg_size = 1024;

  const std::string& d_file;

  size_t d_workers_per_core;
  size_t d_max_msg_size;

  std::mutex d_mtx;
};

} /* namespace core */
} /* namespace eqnx */

#endif /* CORE_INCLUDE_EQUINOX_CORE_SETTINGS_H_ */
