/*
 * Equinox: SDR platform for realtime applications
 * Copyright (C) 2017  Manolis Surligas <surligas@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef CORE_INCLUDE_EQUINOX_THREADING_CPUSET_H_
#define CORE_INCLUDE_EQUINOX_THREADING_CPUSET_H_

#include <cstdint>
#include <vector>
#include <sched.h>
#include <thread>

namespace eqnx
{

namespace core
{

/**
 * @ingroup threading
 *
 * This class is a convenient wrapper around the CPU_SET() macros defined
 * in the \ref <sched.h>.
 *
 * It provides methods for setting properly a cpu_set_t variable by just
 * using a std::vector describing which core should be used and which not.
 */
class cpuset
{
public:
  cpuset ();
  ~cpuset ();

  static uint32_t
  cpu_avail ();

  void
  set_affinity (std::thread& t, std::vector<uint8_t> cpu);

  std::vector<uint8_t>
  get_affinity ();

  int
  count ();

  bool
  is_affinity_set ();

private:
  const uint32_t d_total_cpus;
  bool d_affinity_set;
  int d_cpu_cnt;
  cpu_set_t *d_set;
  std::vector<uint8_t> d_cpu_map;
};

}  // namespace core

}  // namespace eqnx

#endif /* CORE_INCLUDE_EQUINOX_THREADING_CPUSET_H_ */
