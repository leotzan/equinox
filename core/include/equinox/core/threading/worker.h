/*
 * Equinox: SDR platform for realtime applications
 * Copyright (C) 2017  Manolis Surligas <surligas@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef CORE_INCLUDE_EQUINOX_THREADING_WORKER_H_
#define CORE_INCLUDE_EQUINOX_THREADING_WORKER_H_

#include <equinox/core/sched/inner_sched.h>
#include <equinox/core/threading/cpuset.h>

#include <cstddef>
#include <string>
#include <memory>
#include <type_traits>

namespace eqnx
{

namespace core
{

template<class T>
  class worker
  {
  public:
    static_assert (std::is_base_of<eqnx::core::inner_sched, T>::value,
        "T must extend eqnx::inner_sched");

    worker (const std::string& name, size_t id);

    worker(const worker<T>& ) = delete;

    ~worker ();

    void
    set_kernels (const std::deque<kernel::sptr> kernels);

    void
    stop ();

    void
    start ();

    void
    join ();

    void
    set_affinity (const std::vector<uint8_t>& map);

    std::vector<uint8_t>
    get_affinity ();

    bool
    is_affinity_set ();

    size_t
    id ();

    const std::string&
    name ();

  private:
    const std::string d_name;
    const size_t d_id;
    size_t d_kernel_cnt;
    cpuset d_cpuset;
    std::shared_ptr<inner_sched> d_inner_sched;
    bool d_kernels_set;
    bool d_thrd_stopped;
    std::thread d_t;

    static void
    thread_wrapper (std::shared_ptr<inner_sched> s)
    {
      s->run ();
    }
  };

/**
 * Constructs a worker object that can execute on a single thread. On the
 * worker an arbitrary number of kernels may be assigned
 * @param name the name of the worker
 * @param id the ID of the worker. Should be unique on the process context due
 * to the fact that the eqnx::core::sched::outer_sched performs indexing based
 * on this ID
 */
template<class T>
  inline
  worker<T>::worker (const std::string& name, size_t id) :
          d_name (name),
          d_id (id),
          d_kernel_cnt (0),
          d_cpuset (),
          d_inner_sched (T::make_shared ()),
          d_kernels_set (false),
          d_thrd_stopped (true)
  {
  }

/**
 * Stops the execution of the worker
 */
template<class T>
  inline void
  worker<T>::stop ()
  {
    d_inner_sched->stop ();
    if(d_t.joinable()) {
      d_t.join ();
    }
    d_thrd_stopped = true;
  }

template<class T>
  worker<T>::~worker ()
  {
    if (!d_thrd_stopped) {
      stop ();
    }
  }

/**
 * Assign to the worker a set of kernels
 * @param kernels a list with the kernels to be assigned to the worker
 */
template<class T>
  inline void
  worker<T>::set_kernels (const std::deque<kernel::sptr> kernels)
  {
    for (kernel::sptr i : kernels) {
      kernel_runnable::sptr runnable = kernel_runnable::make_shared (
          i, d_kernel_cnt, d_id);
      d_kernel_cnt++;
      d_inner_sched->insert_kernel (runnable);
    }
    d_inner_sched->set_ready ();
  }

/**
 * Starts the execution of the worker
 */
template<class T>
  inline void
  worker<T>::start ()
  {
    d_t = std::thread(worker::thread_wrapper, d_inner_sched);
    d_inner_sched->start ();
    d_thrd_stopped = false;
  }

template<class T>
  inline void
  worker<T>::join ()
  {
    if (d_t.joinable ()) {
      d_t.join ();
    }
  }

/**
 * Set the CPU affinity of the worker thread
 * @param map a vector describing which processors this worker may use.
 * E.g [0 0 1 1] means that processor 2 and 3 may be used
 */
template<class T>
  inline void
  worker<T>::set_affinity (const std::vector<uint8_t>& map)
  {
    d_cpuset.set_affinity (d_t, map);
  }

/**
 *
 * @return the CPU affinity mapping
 */
template<class T>
  inline std::vector<uint8_t>
  worker<T>::get_affinity ()
  {
    return d_cpuset.get_affinity ();
  }

/**
 *
 * @return true if the CPU affinity is set, false otherwise. When not set,
 * worker can operate on any available processor
 */
template<class T>
  inline bool
  worker<T>::is_affinity_set ()
  {
    return d_cpuset.is_affinity_set ();
  }

/**
 *
 * @return the ID of the worker
 */
template<class T>
  inline size_t
  worker<T>::id ()
  {
    return d_id;
  }

/**
 *
 * @return the name of the worker
 */
template<class T>
  inline const std::string&
  worker<T>::name ()
  {
    return d_name;
  }

}  // namespace core

}  // namespace eqnx

#endif /* CORE_INCLUDE_EQUINOX_THREADING_WORKER_H_ */
