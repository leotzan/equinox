/*
 * Equinox: SDR platform for realtime applications
 * Copyright (C) 2017  Manolis Surligas <surligas@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef CORE_INCLUDE_EQUINOX_MSG_MSG_H_
#define CORE_INCLUDE_EQUINOX_MSG_MSG_H_

#include <equinox/core/core.h>

#include <memory>

namespace eqnx
{
/**
 * @defgroup msg
 * @ingroup msg
 *
 * Equinox makes use of messages to exchange data between the processing
 * blocks. Upon each message creation no memory is allocated. Each message
 * contains a reference to a pre-allocated memory region that is managed from
 * the global memory manager. Based on the processing block requirements,
 * the memory region can be a range of shared memory address space or a
 * thread local one.
 *
 * From the user perspective, this class totally abstracts the memory type.
 */
class msg
{
public:
  /**
   * Shared pointer of a message
   */
  typedef std::shared_ptr<msg> sptr;

  static sptr
  make_shared (size_t key, void * const mem, size_t size,
               std::shared_ptr<core::mem_pool> pool);

  ~msg ();

  size_t
  key ();

  void *
  raw_ptr ();

private:
  msg (size_t key, void * const mem, size_t size,
       std::shared_ptr<core::mem_pool> pool);

  const size_t d_key;
  const size_t d_max_size;
  std::shared_ptr<core::mem_pool> d_mem_pool;
  void * const d_ptr;
  size_t d_nsamples;
};

}  // namespace eqnx

#endif /* CORE_INCLUDE_EQUINOX_MSG_MSG_H_ */
