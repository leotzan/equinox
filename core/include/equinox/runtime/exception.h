/*
 * Equinox: SDR platform for realtime applications
 * Copyright (C) 2017  Manolis Surligas <surligas@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef CORE_INCLUDE_EQUINOX_RUNTIME_EXCEPTION_H_
#define CORE_INCLUDE_EQUINOX_RUNTIME_EXCEPTION_H_

#include <exception>

namespace eqnx
{

/**
 * @ingroup runtime
 */
class exception : public std::exception
{
public:
  typedef enum
  {
    PORT_NOT_FOUND = 0,
    INPUT_PORT_NOT_FOUND,
    OUTPUT_PORT_NOT_FOUND,
    OUTPUT_PORT_NOT_CONFIGURED,
    SRC_PORT_IS_NOT_OUTPUT,
    DST_PORT_IS_NOT_INPUT,
    SRC_PORT_DOWNCAST_ERROR,
    DST_PORT_DOWNCAST_ERROR,
    CONNECTION_EXISTS_ERROR,
    LOAD_BALANCER_ANALYSIS_NOT_PERFORMED,
    LOAD_BALANCER_INVALID_ANALYSIS,
    LOAD_BALANCER_TOO_FEW_WORKERS,
    INVALID_GRAPH,
    INVALID_GRAPH_OP,
    HARDWARE_CONCURENCY_INVALID,
    CPU_MAP_SIZE_INVALID
  } code_t;

  exception (code_t c);

  const char *
  what () const throw ();

private:
  code_t d_code;
};

}  // namespace eqnx

#endif /* CORE_INCLUDE_EQUINOX_RUNTIME_EXCEPTION_H_ */
