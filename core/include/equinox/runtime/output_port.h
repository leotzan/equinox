/*
 * Equinox: SDR platform for realtime applications
 * Copyright (C) 2017  Manolis Surligas <surligas@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef CORE_INCLUDE_EQUINOX_RUNTIME_OUTPUT_PORT_H_
#define CORE_INCLUDE_EQUINOX_RUNTIME_OUTPUT_PORT_H_

#include <equinox/core/core.h>
#include <equinox/runtime/io_port.h>
#include <equinox/runtime/input_port.h>
#include <equinox/core/mm/mem_pool.h>
#include <equinox/core/mm/msg_queue.h>

namespace eqnx
{

/**
 * @ingroup runtime
 */
class output_port : public virtual io_port
{
public:
  typedef std::shared_ptr<output_port> sptr;

  static sptr
  make_shared (const std::string& name, size_t capacity,
               size_t msg_size = 2048);

  ~output_port ();

  void
  setup (core::mem_pool::sptr mp, core::msg_queue::sptr mq);

  msg::sptr
  new_msg ();

  int
  write(msg::sptr m);

  template<typename _Rep>
    int
    write (msg::sptr m, const std::chrono::duration<_Rep>& timeout);

  size_t
  capacity ();

  size_t
  msg_size ();

  bool
  full();

  void
  stop ();

  friend std::shared_ptr<core::connection>
  operator >> (output_port::sptr out, input_port::sptr in)
  {
    /* FIXME! */
    return nullptr;
  }

protected:
  output_port (const std::string& name, size_t capacity, size_t msg_size);

private:

  size_t d_capacity;
  size_t d_msg_size;

  /**
   * The message queue that corresponds to the output port
   */
  core::msg_queue::sptr d_mq;
  core::mem_pool::sptr d_mp;
};

}  // namespace eqnx

#endif /* CORE_INCLUDE_EQUINOX_RUNTIME_OUTPUT_PORT_H_ */
