/*
 * Equinox: SDR platform for realtime applications
 * Copyright (C) 2017  Manolis Surligas <surligas@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef CORE_INCLUDE_EQUINOX_UTILS_H_
#define CORE_INCLUDE_EQUINOX_UTILS_H_

#include <equinox/log.h>

/**
 * @param val the number to be rounded up to the next power of 2
 * @return the next power of 2, or the number itself if it is already a power
 * of 2.
 */
static inline size_t
get_next_pow2 (size_t val)
{
  size_t log2 = 0;
  if (val < 1) {
    return 2;
  }
  /* Find the next power of two */
  if ( (val & (val - 1)) != 0) {
    while (val >>= 1) {
      log2++;
    }
    val = 1 << (log2 + 1);
  }
  return val;
}


#endif /* CORE_INCLUDE_EQUINOX_UTILS_H_ */
