/*
 * Equinox: SDR platform for realtime applications
 * Copyright (C) 2017  Manolis Surligas <surligas@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <equinox/core/mm/mem_pool_r.h>

namespace eqnx
{

namespace core
{

mem_pool::sptr
mem_pool_r::make_shared (size_t msg_num, size_t max_msg_size)
{
  return std::shared_ptr<mem_pool> (new mem_pool_r (msg_num, max_msg_size));
}

mem_pool_r::mem_pool_r (size_t msg_num, size_t max_msg_size) :
        mem_pool (msg_num, max_msg_size)
{
  /* Allocate shared memory */
  /*
   * TODO: Allocate proper memory depending the threads that exchange
   * messages
   */
  for (size_t i = 0; i < msg_num; i++) {
    d_blocks[i].ptr = new uint8_t[max_msg_size];
  }

}

mem_pool_r::~mem_pool_r ()
{
  for (size_t i = 0; i < d_blocks_num; i++) {
    delete[] (uint8_t *) d_blocks[i].ptr;
  }
}

msg::sptr
mem_pool_r::new_msg ()
{
  std::unique_lock<std::mutex> lock (d_mtx);
  while (d_free_blocks.empty ()) {
    d_cv.wait (lock);
  }
  mem_block_t *b = d_free_blocks.pop (0);
  d_cv.notify_all ();
  return msg::make_shared (b->key, b->ptr, d_block_size, shared_from_this ());
}

void
mem_pool_r::release (size_t key)
{
  std::unique_lock<std::mutex> lock (d_mtx);
  while (d_free_blocks.full ()) {
    d_cv.wait (lock);
  }
  d_free_blocks.push (&d_blocks[key]);
  d_cv.notify_all ();
}

}  // namespace core

}  // namespace eqnx
