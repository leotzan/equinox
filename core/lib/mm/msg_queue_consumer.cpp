/*
 * Equinox: SDR platform for realtime applications
 * Copyright (C) 2017  Manolis Surligas <surligas@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <equinox/core/mm/msg_queue_consumer.h>
#include <equinox/core/mm/msg_queue.h>

namespace eqnx
{

namespace core
{

/**
 * Creates a message queue consumer that is subscribed automatically at
 * the message queue \a msgq
 * @param name the mnemonic name of the message queue consumer
 * @param id the ID of the consumer on the message queue
 * @param msgq the message queue to register at
 * @return a msg_queue_consumer::uptr to the consumer object
 */
std::unique_ptr<msg_queue_consumer>
msg_queue_consumer::make (const std::string& name, size_t id,
                          std::shared_ptr<msg_queue> msgq)
{
  return std::unique_ptr<msg_queue_consumer> (
      new msg_queue_consumer (name, id, msgq));
}

/**
 * Destructs the consumer object and un-subscribe it from the message
 * queue that is assigned with
 */
msg_queue_consumer::~msg_queue_consumer ()
{
}

/**
 * Reads a message from the message queue. Depending the underlying
 * message queue implementation this method can block waiting for a
 * message to be available, or just return a nullptr when no messages are
 * available.
 *
 * @return a msg::sptr with the first available message on the message
 * queue, or nullptr if no message was available
 */
msg::sptr
msg_queue_consumer::read ()
{
  return d_msgq->pop (d_id);
}

/**
 * Reads a message from the message queue. Depending the underlying
 * message queue implementation this method can block waiting for a
 * message to be available, or just return a nullptr when no messages are
 * available. If the method blocks, it is guaranteed that it would block
 * for a minimum duration specified by timeout. In the timer expires,
 * the method returns a nullptr
 *
 * @param timeout the minimum duration to block waiting for a message to
 * arrive. Note that in case the implementation of the underline message
 * queue does not support blocking, this method will return immediately
 * a nullptr in case no message was available.
 *
 * @return a msg::sptr with the first available message on the message
 * queue, or nullptr if no message was available or the timer expired
 */
template<typename _Rep>
  msg::sptr
  msg_queue_consumer::read (const std::chrono::duration<_Rep>& timeout)
  {
    int64_t us =
        std::chrono::duration_cast<std::chrono::microseconds> (timeout).count ();
    return
        us < 1 ?
            d_msgq->pop_nowait (d_id) :
            d_msgq->pop_timedwait (d_id, (size_t) us);
  }

/**
 *
 * @return the name of the consumer
 */
const std::string
msg_queue_consumer::name ()
{
  return d_name;
}

/**
 *
 * @return true if the message queue of the consumer is empty,
 * false otherwise
 */
bool
msg_queue_consumer::empty()
{
  return d_msgq->empty(d_id);
}

/**
 * @return the ID of the consumer on the message queue
 */
size_t
msg_queue_consumer::id ()
{
  return d_id;
}

/**
 * Creates a message queue consumer that is subscribed automatically at
 * the message queue \a msgq
 * @param name the name of the message queue consumer
 * @param id  the ID of the consumer on the message queue
 * @param msgq the message queue shared pointer
 */
msg_queue_consumer::msg_queue_consumer (const std::string& name, size_t id,
                                        msg_queue::sptr msgq) :
        d_name (name),
        d_id (id),
        d_msgq (msgq)
{
}

}  // namespace core

}  // namespace eqnx
