/*
 * Equinox: SDR platform for realtime applications
 * Copyright (C) 2017  Manolis Surligas <surligas@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <equinox/core/mm/msg_queue_simple.h>

namespace eqnx
{

namespace core
{

msg_queue::sptr
msg_queue_simple::make_shared (const std::string& name, size_t size,
                               size_t consumers, bool allow_coalescing,
                               bool profiling_mode)
{
  return std::shared_ptr<msg_queue> (
      new msg_queue_simple (name, size, consumers, allow_coalescing,
                            profiling_mode));
}

msg_queue_simple::msg_queue_simple (const std::string& name, size_t size,
                                    size_t consumers, bool allow_coalescing,
                                    bool profiling_mode) :
        msg_queue (name, size, consumers, allow_coalescing, profiling_mode)
{
}

int
msg_queue_simple::push (msg::sptr msg)
{
  if (d_stop) {
    throw msg_queue_exception ();
  }
  if (d_q.full ()) {
    return EQNX_MSGQ_NO_MEM;
  }
  d_q.push (msg);
  return EQNX_MSGQ_NO_ERROR;
}

int
msg_queue_simple::push_timedwait (msg::sptr msg, size_t timeout_us)
{
  return push (msg);
}

int
msg_queue_simple::push_nowait (msg::sptr msg)
{
  return push (msg);
}

msg::sptr
msg_queue_simple::pop (size_t reader)
{
  if (d_stop) {
    throw msg_queue_exception ();
  }
  if (d_q.empty ()) {
    return nullptr;
  }
  return d_q.pop (reader);
}

msg::sptr
msg_queue_simple::pop_timedwait (size_t reader, size_t timeout_us)
{
  return pop (reader);
}

msg::sptr
msg_queue_simple::pop_nowait (size_t reader)
{
  return pop (reader);
}

void
msg_queue_simple::clear ()
{
  while (!d_q.empty ()) {
    for (size_t i = 0; i < d_consumers_num; i++) {
      if (!d_q.empty (i)) {
        msg::sptr x = d_q.pop (i);
      }
    }
  }
}

bool
msg_queue_simple::empty ()
{
  return d_q.empty();
}

bool
msg_queue_simple::empty (size_t reader)
{
  return d_q.empty(reader);
}

bool
msg_queue_simple::full ()
{
  return d_q.full();
}

msg_queue_simple::~msg_queue_simple ()
{
  clear ();
}

}  // namespace core

}  // namespace eqnx

