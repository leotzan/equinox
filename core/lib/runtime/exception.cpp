/*
 * Equinox: SDR platform for realtime applications
 * Copyright (C) 2017  Manolis Surligas <surligas@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <equinox/runtime/exception.h>

namespace eqnx
{
exception::exception (code_t c) :
        d_code (c)
{
}

const char*
exception::what () const throw ()
{
  switch (d_code)
    {
    case PORT_NOT_FOUND:
      return "Port does not exist";
    case INPUT_PORT_NOT_FOUND:
      return "Input port does not exist";
    case OUTPUT_PORT_NOT_FOUND:
      return "Output port does not exist";
    case OUTPUT_PORT_NOT_CONFIGURED:
      return "Output port has not an assigned message queue";
    case SRC_PORT_IS_NOT_OUTPUT:
      return "Source port is not configured as output";
    case DST_PORT_IS_NOT_INPUT:
      return "Destination port is not configured as input";
    case SRC_PORT_DOWNCAST_ERROR:
      return "Source port could not be created from generic IO port object";
    case DST_PORT_DOWNCAST_ERROR:
      return "Destination port could not be created from generic IO port object";
    case CONNECTION_EXISTS_ERROR:
      return "The connection already exists";
    case LOAD_BALANCER_ANALYSIS_NOT_PERFORMED:
      return "Graph analysis has not yet performed. Consider calling the analyze () method";
    case LOAD_BALANCER_INVALID_ANALYSIS:
      return "Load balancer invalid graph analysis";
    case LOAD_BALANCER_TOO_FEW_WORKERS:
      return "The number of workers cannot satisfy the number of the graph's connected components";
    case INVALID_GRAPH:
      return "Invalid graph. Probably it contains cycles";
    case INVALID_GRAPH_OP:
      return "Invalid graph operation";
    case HARDWARE_CONCURENCY_INVALID:
      return "Failed to retrieve the number of available CPUs";
    case CPU_MAP_SIZE_INVALID:
      return "The size of the CPU mapping does not conform with the number of available cores";
    default:
      return "Unknown Equinox runtime exception";
    }
}

}  // namespace eqnx
