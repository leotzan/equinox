/*
 * Equinox: SDR platform for realtime applications
 * Copyright (C) 2017, 2018  Manolis Surligas <surligas@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <equinox/runtime/kernel.h>
#include <equinox/runtime/exception.h>

namespace eqnx
{

/**
 * Reads a message from a port
 * @param port_name the port name
 * @return a msg shared pointer
 */
msg::sptr
kernel::read (const std::string& port_name)
{
  return read(input(port_name));
}

/**
 * Reads a message from a port
 * @param p the input port
 * @return a msg shared pointer
 */
msg::sptr
kernel::read (input_port::sptr p)
{
  return p->read();
}

template<typename _Rep>
  inline msg::sptr
  kernel::read (const std::string& port_name,
                const std::chrono::duration<_Rep>& timeout)
  {
    return read(input(port_name), timeout);
  }

template<typename _Rep>
  inline msg::sptr
  kernel::read (input_port::sptr p,
                const std::chrono::duration<_Rep>& timeout)
  {
    return p->read(timeout);
  }

kernel::kernel (const std::string& name) :
        d_name (name),
        d_running (false)
{
}

kernel::~kernel ()
{
}

/**
 *
 * @return the name of the kernel
 */
const std::string&
kernel::name () const
{
  return d_name;
}

/**
 * Creates a new input port
 * @param name the name of the input port. This method throws an exception if
 * the kernel is already in the runtime phase or the port name already exists
 */
void
kernel::new_input_port (const std::string& name)
{
  if (d_running) {
    throw std::runtime_error (
        "Port configuration is not allowed during runtime");
  }

  /* Check if there is already a port with the same name */
  if (d_in_ports.find (name) != d_in_ports.end ()) {
    throw std::invalid_argument ("Port already exists");
  }
  d_in_ports[name] = input_port::make_shared (name);
  d_input_ports_list.push_back(d_in_ports[name]);
}

/**
 * Creates a new output port
 * @param name the name of the output port. This method throws an exception if
 * the kernel is already in the runtime phase or the port name already exists
 */
void
kernel::new_output_port (const std::string& name, size_t msg_num,
                         size_t msg_size)
{
  if (d_running) {
    throw std::runtime_error (
        "Port configuration is not allowed during runtime");
  }

  /* Check if there is already a port with the same name */
  if (d_out_ports.find (name) != d_out_ports.end ()) {
    throw std::invalid_argument ("Port already exists");
  }
  d_out_ports[name] = output_port::make_shared (name, msg_num, msg_size);
  d_output_ports_list.push_back(d_out_ports[name]);
}

/**
 * Get a registered input port
 *
 * @note The input port may not yet valid. Use the ready() method to ensure
 * that the kernel can be executed.
 *
 * @param name the name of the port
 * @return shared pointer of the port
 */
input_port::sptr
kernel::input (const std::string& name)
{
  auto f = d_in_ports.find (name);
  if (f == d_in_ports.end ()) {
    throw exception (exception::INPUT_PORT_NOT_FOUND);
  }
  return f->second;
}

std::deque<input_port::sptr>
kernel::inputs ()
{
  return d_input_ports_list;
}

/**
 * Get a registered output port
 *
 * @note The output port may not yet valid. Use the ready() method to ensure
 * that the kernel can be executed.
 * @param name the name of the port
 * @return shared pointer of the port
 */
output_port::sptr
kernel::output (const std::string& name)
{
  auto f = d_out_ports.find (name);
  if (f == d_out_ports.end ()) {
    throw exception (exception::OUTPUT_PORT_NOT_FOUND);
  }
  return f->second;
}

std::deque<output_port::sptr>
kernel::outputs ()
{
  return d_output_ports_list;
}

/**
 * @return true if the kernel is ready to be executed through the exec()
 * method, false otherwise.
 *
 * @note A kernel is considered ready, when all of the input and output ports
 * have been properly configured and the corresponding message pools and/or
 * message queues have been successfully assigned.
 */
bool
kernel::ready()
{
  for(auto i : d_in_ports) {
    if(!i.second->is_ready()) {
      return false;
    }
  }

  for(auto i : d_out_ports) {
    if(!i.second->is_ready()) {
      return false;
    }
  }
  return true;
}

/**
 * Assigns to the input port \a port_name the message queue \a mq
 * @param port_name the port name
 * @param mq the message queue to read messages from. The implementation of the
 * message queue depends on the execution environment
 */
void
kernel::setup_input_port (const std::string& port_name,
                          core::msg_queue::sptr mq)
{
  input_port::sptr in = input(port_name);
  in->setup(mq);
}

/**
 * Assigns to the output port \a port_name the message pool \a mp and the
 * message queue \a mq
 * @param port_name the port name
 * @param mp a shared pointer to a message pool. The implementation of the
 * message pool depends on the execution environment
 * @param mq a shared pointer to a message queue. The implementation of the
 * message queue depends on the execution environment
 */
void
kernel::setup_output_port (const std::string& port_name,
                           core::mem_pool::sptr mp, core::msg_queue::sptr mq)
{
  output_port::sptr out = output(port_name);
  out->setup(mp, mq);
}

size_t
kernel::input_num ()
{
  return d_in_ports.size ();
}

size_t
kernel::output_num ()
{
  return d_out_ports.size ();
}

bool
kernel::is_source ()
{
  return d_in_ports.size () == 0;
}

bool
kernel::is_sink ()
{
  return d_out_ports.size () == 0;
}

std::ostream&
operator<< (std::ostream& out, const kernel& y)
{
  return out << y.d_name;
}

core::connection_anchor
kernel::operator [] (const std::string& port_name)
{
  std::shared_ptr<io_port> p;
  try {
    p = input (port_name);
  }
  catch (exception& e1) {
    try {
      p = output (port_name);
    }
    catch (exception& e2) {
      throw exception (exception::PORT_NOT_FOUND);
    }
  }
  return core::connection_anchor (shared_from_this (), p);
}

core::connection_anchor
kernel::port (const std::string& port_name)
{
  std::shared_ptr<io_port> p;
  try {
    p = input (port_name);
  }
  catch (exception& e1) {
    try {
      p = output (port_name);
    }
    catch (exception& e2) {
      throw exception (exception::PORT_NOT_FOUND);
    }
  }
  return core::connection_anchor (shared_from_this (), p);
}

msg::sptr
kernel::new_msg (const std::string& port_name)
{
  output_port::sptr p = output(port_name);
  return p->new_msg();
}

msg::sptr
kernel::new_msg (output_port::sptr p)
{
  return p->new_msg();
}

int
kernel::write (const std::string& port_name, msg::sptr m)
{
  output_port::sptr p = output(port_name);
  return p->write(m);
}

int
kernel::write (output_port::sptr p, msg::sptr m)
{
  return p->write(m);
}

}  // namespace eqnx

