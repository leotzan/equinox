/*
 * Equinox: SDR platform for realtime applications
 * Copyright (C) 2017  Manolis Surligas <surligas@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <equinox/runtime/kernel_runnable.h>

namespace eqnx
{

/**
 *
 * Construct a kernel runnable object
 * @param k the kernel shared pointer
 * @param id the ID of the kernel
 * @param worker_id the worker ID that the kernel belongs to
 * @return a shared pointer to a eqnx::kernel_runnable object
 */
kernel_runnable::sptr
kernel_runnable::make_shared (kernel::sptr k, size_t id, size_t worker_id)
{
  return std::shared_ptr<kernel_runnable> (
      new kernel_runnable (k, id, worker_id));
}

/**
 * Construct a kernel runnable object
 * @param k the kernel shared pointer
 * @param id the ID of the kernel
 * @param worker_id the worker ID that the kernel belongs to
 */
kernel_runnable::kernel_runnable (kernel::sptr k, size_t id, size_t worker_id) :
        d_k (k),
        d_id (id),
        d_worker_id (worker_id)
{
}

/**
 *
 * @return the ID of this kernel.
 */
const size_t
kernel_runnable::id () const
{
  return d_id;
}

/**
 *
 * @return the ID of the worker that this kernel is assigned to.
 */
const size_t
kernel_runnable::worker_id () const
{
  return d_worker_id;
}

/**
 *
 * @return the kernel shared pointer assign to the this runnable object
 */
const kernel::sptr
kernel_runnable::get_kernel ()
{
  return d_k;
}

void
kernel_runnable::exec ()
{
  d_k->exec ();
}

}  // namespace eqnx

