/*
 * Equinox: SDR platform for realtime applications
 * Copyright (C) 2017  Manolis Surligas <surligas@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <equinox/core/sched/connection_anchor.h>
#include <equinox/core/sched/connection.h>

namespace eqnx
{

namespace core
{

/**
 * Default contructor setting the kernel and the port of the
 * corresponding kernel to an initial invalid value
 */
connection_anchor::connection_anchor () :
        d_kernel (nullptr),
        d_port (nullptr)
{
}

/**
 * Creates a new connection endpoint
 * @param k the kernel
 * @param port the port of the corresponding kernel
 */
connection_anchor::connection_anchor (std::shared_ptr<kernel> k,
                                      std::shared_ptr<io_port> port) :
        d_kernel (k),
        d_port (port)
{
}

/**
 *
 * @return the kernel shared pointer of the connection endpoint
 */
std::shared_ptr<kernel>
core::connection_anchor::get_kernel () const
{
  return d_kernel;
}

/**
 *
 * @return the port shared pointer of the connection endpoint
 */
std::shared_ptr<io_port>
connection_anchor::get_port () const
{
  return d_port;
}

/**
 * Creates a new connection based on two connection endpoints
 * @param anchor0 the source endpoint
 * @param anchor1 the destination endpoint
 * @return shared pointer with a new connection object
 */
std::shared_ptr<connection>
operator >> (const connection_anchor& anchor0, const connection_anchor& anchor1)
{
  return connection::make_shared (anchor0, anchor1);
}

}  // namespace core

}  // namespace eqnx

