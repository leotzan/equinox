/*
 * Equinox: SDR platform for realtime applications
 * Copyright (C) 2017  Manolis Surligas <surligas@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <equinox/core/sched/load_balancer_uniform.h>

namespace eqnx
{

namespace core
{
load_balancer_uniform::load_balancer_uniform (connection_graph::sptr graph,
                                              size_t workers_num) :
        load_balancer (graph, workers_num)
{
}

bool
load_balancer_uniform::analyze ()
{
  /* TODO */
  return false;
}

void
load_balancer_uniform::reset ()
{
  d_graph_analyzed = false;
  /* TODO */
}

std::deque<kernel::sptr>
load_balancer_uniform::get_worker_assignement (size_t index)
{
  std::deque<kernel::sptr> x;
  /* TODO */
  return x;
}

bool
load_balancer_uniform::find_kernel (const size_t index, const kernel::sptr k)
{
  /* TODO */
  return false;
}

}  // namespace core

}  // namespace eqnx

