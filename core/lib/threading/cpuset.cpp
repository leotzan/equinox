/*
 * Equinox: SDR platform for realtime applications
 * Copyright (C) 2017  Manolis Surligas <surligas@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <equinox/core/threading/cpuset.h>
#include <equinox/runtime/exception.h>

namespace eqnx
{

namespace core
{

/**
 *
 * @return the number of available CPUs
 */
uint32_t
cpuset::cpu_avail ()
{
  return std::thread::hardware_concurrency ();
}

/**
 * Allocates all the necessary structures for the affinity management
 */
cpuset::cpuset () :
        d_total_cpus (std::thread::hardware_concurrency ()),
        d_affinity_set (false),
        d_cpu_cnt (d_total_cpus),
        /* By default use all available CPUs*/
        d_cpu_map (d_total_cpus, 1)
{
  if (!d_total_cpus) {
    throw exception (exception::HARDWARE_CONCURENCY_INVALID);
  }

  d_set = CPU_ALLOC(CPU_ALLOC_SIZE(d_total_cpus));
  if (!d_set) {
    throw std::runtime_error ("Could not allocate memory for the cpu_set_t");
  }

  CPU_ZERO_S(CPU_ALLOC_SIZE(d_total_cpus), d_set);
  for (uint32_t i = 0; i < d_total_cpus; i++) {
    CPU_SET_S(i, CPU_ALLOC_SIZE(d_total_cpus), d_set);
  }
}

cpuset::~cpuset ()
{
  CPU_FREE(d_set);
}

/**
 * Set the affinity of a thread
 * @param t the thread to set its affinity. Note that the thread should
 * be already running in order this method to have an effect on it
 * @param cpu the CPU mapping. The mapping is a vector with a number of
 * elements equal to the number of available CPUs.
 * Each element should be either 0, meaning that the corresponding CPU
 * will be not used, or 1 otherwise
 */
void
cpuset::set_affinity (std::thread& t, std::vector<uint8_t> cpu)
{
  if (cpu.size () != d_total_cpus) {
    throw exception (exception::CPU_MAP_SIZE_INVALID);
  }

  CPU_ZERO_S(CPU_ALLOC_SIZE(d_total_cpus), d_set);
  for (uint32_t i = 0; i < d_total_cpus; i++) {
    CPU_SET_S(cpu[i], CPU_ALLOC_SIZE(d_total_cpus), d_set);
  }
  d_cpu_map = cpu;
  pthread_setaffinity_np (t.native_handle (), CPU_ALLOC_SIZE(d_total_cpus),
                          d_set);
  d_affinity_set = true;
}

/**
 *
 * @return a vector with the current CPU mapping
 */
std::vector<uint8_t>
cpuset::get_affinity ()
{
  return d_cpu_map;
}

/**
 *
 * @return the number of the active CPUs in the set. This method is
 * just a wrapper of the CPU_COUNT_S() function
 */
int
cpuset::count ()
{
  return CPU_COUNT_S(CPU_ALLOC_SIZE(d_total_cpus), d_set);
}

bool
cpuset::is_affinity_set ()
{
  return d_affinity_set;
}

}  // namespace core

}  // namespace eqnx

