/*
 * Equinox: SDR platform for realtime applications
 * Copyright (C) 2017  Manolis Surligas <surligas@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef CORE_TEST_TEST_CIRC_BUFFER_H_
#define CORE_TEST_TEST_CIRC_BUFFER_H_

#include <cppunit/TestFixture.h>
#include <cppunit/extensions/HelperMacros.h>

#include <equinox/core/mm/circ_buffer.h>
namespace eqnx
{

namespace core
{

class test_circ_buffer : public CppUnit::TestFixture
{
CPPUNIT_TEST_SUITE(test_circ_buffer);
  CPPUNIT_TEST(test_allocation_power_of_2);
  CPPUNIT_TEST(test_allocation_non_power_of_2);
  CPPUNIT_TEST(test_empty);
  CPPUNIT_TEST(test_full);
  CPPUNIT_TEST(test_push_pop);
  CPPUNIT_TEST(test_push_all_pop);
  CPPUNIT_TEST(test_multiple_readers);CPPUNIT_TEST_SUITE_END()
  ;
private:
  circ_buffer<int> *d_int_circ;
public:
  void
  setUp ();
  void
  tearDown ();
  void
  test_allocation_power_of_2 ();
  void
  test_allocation_non_power_of_2 ();
  void
  test_empty ();
  void
  test_full ();
  void
  test_push_pop ();
  void
  test_push_all_pop ();
  void
  test_multiple_readers ();
};

}  // namespace core

}  // namespace eqnx

#endif /* CORE_TEST_TEST_CIRC_BUFFER_H_ */
