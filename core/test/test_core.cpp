/*
 * Equinox: SDR platform for realtime applications
 * Copyright (C) 2017  Manolis Surligas <surligas@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <equinox/core/core.h>
#include "test_circ_buffer.h"
#include "test_connection_graph.h"
#include "test_inner_sched.h"
#include "test_msg_queue.h"
#include "test_mem_pool.h"
#include "test_outer_sched.h"
#include "test_io_ports.h"
#include "test_load_balance.h"
#include "test_threading.h"
#include "test_workers.h"

#include <cppunit/extensions/TestFactoryRegistry.h>
#include <cppunit/ui/text/TestRunner.h>
#include <cppunit/BriefTestProgressListener.h>
#include <cppunit/TestResultCollector.h>
#include <cppunit/TestResult.h>

CPPUNIT_TEST_SUITE_REGISTRATION(eqnx::core::test_circ_buffer);
CPPUNIT_TEST_SUITE_REGISTRATION(eqnx::core::test_connection_graph);
CPPUNIT_TEST_SUITE_REGISTRATION(eqnx::core::test_inner_sched);
CPPUNIT_TEST_SUITE_REGISTRATION(eqnx::core::test_msg_queue);
CPPUNIT_TEST_SUITE_REGISTRATION(eqnx::core::test_mem_pool);
CPPUNIT_TEST_SUITE_REGISTRATION(eqnx::core::test_outer_sched);
CPPUNIT_TEST_SUITE_REGISTRATION(eqnx::test_io_ports);
CPPUNIT_TEST_SUITE_REGISTRATION(eqnx::core::test_load_balance);
CPPUNIT_TEST_SUITE_REGISTRATION(eqnx::core::test_threading);
CPPUNIT_TEST_SUITE_REGISTRATION(eqnx::core::test_workers);

int
main (int argc, char **argv)
{
  CppUnit::TestResult controller;

  CppUnit::TestResultCollector result;
  CppUnit::BriefTestProgressListener progressListener;
  CppUnit::TextUi::TestRunner runner;
  CppUnit::TestFactoryRegistry &registry =
  CppUnit::TestFactoryRegistry::getRegistry ();
  runner.addTest (registry.makeTest ());
  runner.eventManager().addListener(&result);
  runner.eventManager().addListener(&progressListener);
  bool success = runner.run ("", false);
  return success ? 0 : 1;
}




