/*
 * Equinox: SDR platform for realtime applications
 * Copyright (C) 2017  Manolis Surligas <surligas@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "test_inner_sched.h"
#include <equinox/core/sched/inner_sched.h>
#include <equinox/core/sched/inner_sched_rr.h>
#include <equinox/core/threading/worker.h>

#include <equinox/core/mm/msg_queue.h>
#include <equinox/core/mm/mem_pool.h>
#include <equinox/core/mm/msg_queue_simple.h>
#include <equinox/core/mm/mem_pool_simple.h>

#include <equinox/kernels/testing/sink.h>
#include <equinox/kernels/testing/source.h>
#include <equinox/kernels/testing/in_out.h>

namespace eqnx
{

namespace core
{
void
test_inner_sched::test_inner_sched_rr ()
{
  core::msg_queue::sptr mq0 = core::msg_queue_simple::make_shared ("test", 512,
                                                                   1);
  core::msg_queue::sptr mq1 = core::msg_queue_simple::make_shared ("test", 512,
                                                                   1);
  core::msg_queue::sptr mq2 = core::msg_queue_simple::make_shared ("test", 512,
                                                                   1);
  core::msg_queue::sptr mq3 = core::msg_queue_simple::make_shared ("test", 512,
                                                                   1);
  core::msg_queue::sptr mq4 = core::msg_queue_simple::make_shared ("test", 512,
                                                                   1);
  core::mem_pool::sptr mp = core::mem_pool_simple::make_shared (4096, 1024);

  std::deque<kernel::sptr> kernels;
  kernels.push_back (testing::source::make_shared ("s"));
  kernels.push_back (testing::in_out::make_shared ("a"));
  kernels.push_back (testing::in_out::make_shared ("b"));
  kernels.push_back (testing::in_out::make_shared ("c"));
  kernels.push_back (testing::in_out::make_shared ("d"));
  kernels.push_back (testing::sink::make_shared ("e"));

  kernels[0]->setup_output_port("out0", mp, mq0);

  kernels[1]->setup_input_port("in0", mq0);
  kernels[1]->setup_output_port("out0", mp, mq1);

  kernels[2]->setup_input_port("in0", mq1);
  kernels[2]->setup_output_port("out0", mp, mq2);

  kernels[3]->setup_input_port("in0", mq2);
  kernels[3]->setup_output_port("out0", mp, mq3);

  kernels[4]->setup_input_port("in0", mq3);
  kernels[4]->setup_output_port("out0", mp, mq4);

  kernels[5]->setup_input_port("in0", mq4);

  worker<inner_sched_rr> w ("test", 0);
  w.set_kernels (kernels);
  w.start ();
  std::this_thread::sleep_for (std::chrono::seconds (5));
  w.stop ();
}

void
test_inner_sched::test_inner_sched_rr_scoped_exit ()
{
  std::deque<kernel::sptr> kernels;
  kernels.push_back (testing::sink::make_shared ("test"));
  kernels.push_back (testing::sink::make_shared ("test"));
  kernels.push_back (testing::sink::make_shared ("test"));
  kernels.push_back (testing::sink::make_shared ("test"));

  worker<inner_sched_rr> w("test", 0);
  w.set_kernels (kernels);
  w.start ();
  std::this_thread::sleep_for (std::chrono::seconds (1));
}

}  // namespace core

}  // namespace eqnx

