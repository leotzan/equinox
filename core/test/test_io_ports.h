/*
 * Equinox: SDR platform for realtime applications
 * Copyright (C) 2017  Manolis Surligas <surligas@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef CORE_TEST_TEST_IO_PORTS_H_
#define CORE_TEST_TEST_IO_PORTS_H_

#include <cppunit/TestFixture.h>
#include <cppunit/extensions/HelperMacros.h>

#include <equinox/core/mm/mem_pool.h>
#include <equinox/runtime/input_port.h>
#include <equinox/runtime/output_port.h>

namespace eqnx
{

class test_io_ports : public CppUnit::TestFixture
{
CPPUNIT_TEST_SUITE(test_io_ports);
  CPPUNIT_TEST(test_produce_consume);CPPUNIT_TEST_SUITE_END()
  ;
private:
  void
  test_produce_consume ();

  core::mem_pool::sptr d_mp;
  core::msg_queue::sptr d_mq;
  input_port::sptr d_in0;
  input_port::sptr d_in1;
  output_port::sptr d_out;

public:
  void
  setUp ();
  void
  tearDown ();

};

}  // namespace name

#endif /* CORE_TEST_TEST_IO_PORTS_H_ */
