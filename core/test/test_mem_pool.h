/*
 * Equinox: SDR platform for realtime applications
 * Copyright (C) 2017  Manolis Surligas <surligas@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef CORE_TEST_TEST_MEM_POOL_H_
#define CORE_TEST_TEST_MEM_POOL_H_

#include <cppunit/TestFixture.h>
#include <cppunit/extensions/HelperMacros.h>
#include <equinox/core/mm/mem_pool.h>

namespace eqnx
{

namespace core
{

class test_mem_pool : public CppUnit::TestFixture
{
CPPUNIT_TEST_SUITE(test_mem_pool);
  CPPUNIT_TEST(test_mem_pool_simple);
  CPPUNIT_TEST(test_mem_pool_r);CPPUNIT_TEST_SUITE_END()
  ;
private:
  mem_pool::sptr d_mp_simple;
  mem_pool::sptr d_mp_r;

  void
  test_mem_pool_simple ();
  void
  test_mem_pool_r ();

public:
  void
  setUp ();
  void
  tearDown ();

};

}  // namespace core

}  // namespace eqnx

#endif /* CORE_TEST_TEST_MEM_POOL_H_ */
