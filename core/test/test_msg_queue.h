/*
 * Equinox: SDR platform for realtime applications
 * Copyright (C) 2017  Manolis Surligas <surligas@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef CORE_TEST_TEST_MSG_QUEUE_H_
#define CORE_TEST_TEST_MSG_QUEUE_H_

#include <cppunit/TestFixture.h>
#include <cppunit/extensions/HelperMacros.h>
#include <equinox/core/mm/msg_queue.h>
#include <equinox/core/mm/msg_queue_simple.h>
#include <equinox/core/mm/msg_queue_consumer.h>
#include <equinox/core/mm/mem_pool.h>
#include <equinox/core/mm/msg_queue_r.h>

namespace eqnx
{
namespace core
{

class test_msg_queue : public CppUnit::TestFixture
{
CPPUNIT_TEST_SUITE(test_msg_queue);
  CPPUNIT_TEST(test_msg_queue_lock_sinlge_consumer);
  CPPUNIT_TEST(test_msg_queue_lock_multiple_consumers);
  CPPUNIT_TEST(test_msg_queue_lock_multiple_consumers_threaded);
  CPPUNIT_TEST(test_msg_queue_simple);
  CPPUNIT_TEST(test_msg_queue_consumer);CPPUNIT_TEST_SUITE_END()
  ;
private:
  msg_queue::sptr d_q;
  mem_pool::sptr d_mp_simple;
  mem_pool::sptr d_mp_r;
  bool d_running;

  void
  producer ();
  void
  consumer (size_t reader_id);

public:
  void
  setUp ();
  void
  tearDown ();
  void
  test_msg_queue_lock_sinlge_consumer ();
  void
  test_msg_queue_lock_multiple_consumers ();
  void
  test_msg_queue_lock_multiple_consumers_threaded ();
  void
  test_msg_queue_simple ();
  void
  test_msg_queue_consumer ();
};

}  // namespace core

}  // namespace eqnx

#endif /* CORE_TEST_TEST_MSG_QUEUE_H_ */
