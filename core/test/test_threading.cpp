/*
 * Equinox: SDR platform for realtime applications
 * Copyright (C) 2017  Manolis Surligas <surligas@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "test_threading.h"

#include <thread>

namespace eqnx
{
namespace core
{
volatile static bool running;
static void
dummy ()
{
  uint32_t cnt = 0;
  while (running) {
    cnt++;
  }
}

void
test_threading::test_cpuset ()
{
  cpuset s;

  CPPUNIT_ASSERT(s.count () == cpuset::cpu_avail ());
  std::vector<uint8_t> af = s.get_affinity ();
  CPPUNIT_ASSERT(s.count () == af.size ());
  /* Check if the default affinity scheme has been applied */
  for (uint8_t cpu : af) {
    CPPUNIT_ASSERT(cpu == 1);
  }

  for (size_t i = 0; i < af.size (); i++) {
    af[i] = 0;
  }
  running = 1;

  af[0] = 1;
  std::thread t (dummy);
  s.set_affinity (t, af);
  std::this_thread::sleep_for (std::chrono::seconds (5));
  running = 0;
  t.join ();
}

}  // namespace core

}  // namespace eqnx

