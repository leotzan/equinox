/*
 * Equinox: SDR platform for realtime applications
 * Copyright (C) 2018  Manolis Surligas <surligas@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "test_workers.h"

#include <equinox/core/core.h>
#include <equinox/runtime/kernel.h>
#include <equinox/core/sched/inner_sched.h>
#include <equinox/core/sched/inner_sched_rr.h>
#include <equinox/core/threading/worker.h>

#include <equinox/core/mm/msg_queue.h>
#include <equinox/core/mm/mem_pool.h>
#include <equinox/core/mm/msg_queue_simple.h>
#include <equinox/core/mm/msg_queue_r.h>
#include <equinox/core/mm/mem_pool_simple.h>
#include <equinox/core/mm/mem_pool_r.h>

#include <equinox/kernels/testing/sink.h>
#include <equinox/kernels/testing/source.h>
#include <equinox/kernels/testing/in_out.h>


namespace eqnx
{

namespace core
{

void
test_workers::workers_interop ()
{
  std::deque<kernel::sptr> kernels_w0;
  std::deque<kernel::sptr> kernels_w1;
  std::deque<kernel::sptr> kernels_w2;
  std::deque<kernel::sptr> kernels_w3;

  core::msg_queue::sptr mq0_w0 = core::msg_queue_simple::make_shared ("test", 512,
                                                                   1);
  core::msg_queue::sptr mq1_w0 = core::msg_queue_simple::make_shared ("test", 512,
                                                                   1);
  core::msg_queue::sptr mq2_w0 = core::msg_queue_simple::make_shared ("test", 512,
                                                                   1);
  core::msg_queue::sptr mq3_w0 = core::msg_queue_simple::make_shared ("test", 512,
                                                                   1);
  core::msg_queue::sptr mq4_w0 = core::msg_queue_r::make_shared ("test", 512,
                                                                   1);
  core::mem_pool::sptr mp_w0 = core::mem_pool_simple::make_shared (4096, 1024);
  core::mem_pool::sptr mp_w0w1 = core::mem_pool_r::make_shared (4096, 1024);
  core::mem_pool::sptr mp_w1 = core::mem_pool_simple::make_shared (4096, 1024);
  core::mem_pool::sptr mp_w1w2 = core::mem_pool_r::make_shared (4096, 1024);
  core::mem_pool::sptr mp_w2 = core::mem_pool_simple::make_shared (4096, 1024);
  core::mem_pool::sptr mp_w2w3 = core::mem_pool_r::make_shared (4096, 1024);
  core::mem_pool::sptr mp_w3 = core::mem_pool_simple::make_shared (4096, 1024);

  core::msg_queue::sptr mq0_w1 = core::msg_queue_simple::make_shared ("test", 512,
                                                                   1);
  core::msg_queue::sptr mq1_w1 = core::msg_queue_simple::make_shared ("test", 512,
                                                                   1);
  core::msg_queue::sptr mq2_w1 = core::msg_queue_simple::make_shared ("test", 512,
                                                                   1);
  core::msg_queue::sptr mq3_w1 = core::msg_queue_simple::make_shared ("test", 512,
                                                                   1);
  core::msg_queue::sptr mq4_w1 = core::msg_queue_r::make_shared ("test", 512,
                                                                   1);

  core::msg_queue::sptr mq0_w2 = core::msg_queue_simple::make_shared ("test", 512,
                                                                   1);
  core::msg_queue::sptr mq1_w2 = core::msg_queue_simple::make_shared ("test", 512,
                                                                   1);
  core::msg_queue::sptr mq2_w2 = core::msg_queue_simple::make_shared ("test", 512,
                                                                   1);
  core::msg_queue::sptr mq3_w2 = core::msg_queue_simple::make_shared ("test", 512,
                                                                   1);
  core::msg_queue::sptr mq4_w2 = core::msg_queue_r::make_shared ("test", 512,
                                                                   1);

  core::msg_queue::sptr mq0_w3 = core::msg_queue_simple::make_shared ("test", 512,
                                                                   1);
  core::msg_queue::sptr mq1_w3 = core::msg_queue_simple::make_shared ("test", 512,
                                                                   1);
  core::msg_queue::sptr mq2_w3 = core::msg_queue_simple::make_shared ("test", 512,
                                                                   1);
  core::msg_queue::sptr mq3_w3 = core::msg_queue_simple::make_shared ("test", 512,
                                                                   1);

  kernels_w0.push_back (testing::source::make_shared ("s"));
  kernels_w0.push_back (testing::in_out::make_shared ("a"));
  kernels_w0.push_back (testing::in_out::make_shared ("b"));
  kernels_w0.push_back (testing::in_out::make_shared ("c"));
  kernels_w0.push_back (testing::in_out::make_shared ("d"));

  kernels_w0[0]->setup_output_port("out0", mp_w0, mq0_w0);
  kernels_w0[1]->setup_input_port("in0", mq0_w0);
  kernels_w0[1]->setup_output_port("out0", mp_w0, mq1_w0);
  kernels_w0[2]->setup_input_port("in0", mq1_w0);
  kernels_w0[2]->setup_output_port("out0", mp_w0, mq2_w0);
  kernels_w0[3]->setup_input_port("in0", mq2_w0);
  kernels_w0[3]->setup_output_port("out0", mp_w0, mq3_w0);
  kernels_w0[4]->setup_input_port("in0", mq3_w0);
  kernels_w0[4]->setup_output_port("out0", mp_w0w1, mq4_w0);

  kernels_w1.push_back (testing::in_out::make_shared ("e"));
  kernels_w1.push_back (testing::in_out::make_shared ("f"));
  kernels_w1.push_back (testing::in_out::make_shared ("g"));
  kernels_w1.push_back (testing::in_out::make_shared ("h"));
  kernels_w1.push_back (testing::in_out::make_shared ("i"));

  kernels_w1[0]->setup_input_port("in0", mq4_w0);
  kernels_w1[0]->setup_output_port("out0", mp_w1, mq0_w1);
  kernels_w1[1]->setup_input_port("in0", mq0_w1);
  kernels_w1[1]->setup_output_port("out0", mp_w1, mq1_w1);
  kernels_w1[2]->setup_input_port("in0", mq1_w1);
  kernels_w1[2]->setup_output_port("out0", mp_w1, mq2_w1);
  kernels_w1[3]->setup_input_port("in0", mq2_w1);
  kernels_w1[3]->setup_output_port("out0", mp_w1, mq3_w1);
  kernels_w1[4]->setup_input_port("in0", mq3_w1);
  kernels_w1[4]->setup_output_port("out0", mp_w1w2, mq4_w1);

  kernels_w2.push_back (testing::in_out::make_shared ("j"));
  kernels_w2.push_back (testing::in_out::make_shared ("k"));
  kernels_w2.push_back (testing::in_out::make_shared ("l"));
  kernels_w2.push_back (testing::in_out::make_shared ("m"));
  kernels_w2.push_back (testing::in_out::make_shared ("n"));

  kernels_w2[0]->setup_input_port("in0", mq4_w1);
  kernels_w2[0]->setup_output_port("out0", mp_w2, mq0_w2);
  kernels_w2[1]->setup_input_port("in0", mq0_w2);
  kernels_w2[1]->setup_output_port("out0", mp_w2, mq1_w2);
  kernels_w2[2]->setup_input_port("in0", mq1_w2);
  kernels_w2[2]->setup_output_port("out0", mp_w2, mq2_w2);
  kernels_w2[3]->setup_input_port("in0", mq2_w2);
  kernels_w2[3]->setup_output_port("out0", mp_w2, mq3_w2);
  kernels_w2[4]->setup_input_port("in0", mq3_w2);
  kernels_w2[4]->setup_output_port("out0", mp_w2w3, mq4_w2);

  kernels_w3.push_back (testing::in_out::make_shared ("o"));
  kernels_w3.push_back (testing::in_out::make_shared ("p"));
  kernels_w3.push_back (testing::in_out::make_shared ("q"));
  kernels_w3.push_back (testing::in_out::make_shared ("r"));
  kernels_w3.push_back (testing::sink::make_shared ("t"));

  kernels_w3[0]->setup_input_port("in0", mq4_w2);
  kernels_w3[0]->setup_output_port("out0", mp_w3, mq0_w3);
  kernels_w3[1]->setup_input_port("in0", mq0_w3);
  kernels_w3[1]->setup_output_port("out0", mp_w3, mq1_w3);
  kernels_w3[2]->setup_input_port("in0", mq1_w3);
  kernels_w3[2]->setup_output_port("out0", mp_w3, mq2_w3);
  kernels_w3[3]->setup_input_port("in0", mq2_w3);
  kernels_w3[3]->setup_output_port("out0", mp_w3, mq3_w3);
  kernels_w3[4]->setup_input_port("in0", mq3_w3);

  worker<inner_sched_rr>w0("test0", 0);
  worker<inner_sched_rr> w1("test1", 1);
  worker<inner_sched_rr> w2("test2", 2);
  worker<inner_sched_rr> w3("test3", 3);
  w0.set_kernels (kernels_w0);
  w1.set_kernels (kernels_w1);
  w2.set_kernels (kernels_w2);
  w3.set_kernels (kernels_w3);
  w0.start ();
  w1.start ();
  w2.start ();
  w3.start ();
  std::this_thread::sleep_for (std::chrono::seconds (5));
  w0.stop ();
  w1.stop ();
  w2.stop ();
  w3.stop ();
}

}  // namespace core

}  // namespace eqnx

