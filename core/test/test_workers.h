/*
 * Equinox: SDR platform for realtime applications
 * Copyright (C) 2018  Manolis Surligas <surligas@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef CORE_TEST_TEST_WORKERS_H_
#define CORE_TEST_TEST_WORKERS_H_


#include <cppunit/TestFixture.h>
#include <cppunit/extensions/HelperMacros.h>

namespace eqnx
{

namespace core
{

class test_workers : public CppUnit::TestFixture
{
    CPPUNIT_TEST_SUITE(test_workers);
    CPPUNIT_TEST(workers_interop);
    CPPUNIT_TEST_SUITE_END();
public:
  void
  workers_interop ();
};

}  // namespace core


}  // namespace eqnx



#endif /* CORE_TEST_TEST_WORKERS_H_ */
