/*
 * Equinox: SDR platform for realtime applications
 * Copyright (C) 2017  Manolis Surligas <surligas@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <equinox/kernels/testing/in_out.h>

#include <cstring>

namespace eqnx
{

namespace testing
{

in_out::sptr
in_out::make_shared (const std::string& name, size_t n_in_ports,
                     size_t n_out_ports)
{
  return std::shared_ptr<in_out> (new in_out (name, n_in_ports, n_out_ports));
}

in_out::in_out (const std::string& name, size_t n_in_ports, size_t n_out_ports) :
        kernel (name)
{
  if (n_in_ports < 1 || n_out_ports < 1) {
    throw std::invalid_argument (
        "You should declare at least one input and one output port");
  }

  for (size_t i = 0; i < n_in_ports; i++) {
    const std::string pname ("in" + std::to_string (i));
    new_input_port (pname);
  }

  for (size_t i = 0; i < n_out_ports; i++) {
    const std::string pname ("out" + std::to_string (i));
    new_output_port (pname, 256, 1024);
  }
}

void
in_out::exec ()
{
  size_t cnt = 0;

  for(size_t i = 0; i < input_num(); i++) {
    size_t tmp;
    const std::string inname ("in" + std::to_string (i));
    input_port::sptr in = input(inname);
    msg::sptr m = in->read();
    memcpy(&tmp, m->raw_ptr(), sizeof(size_t));
    cnt += tmp;
  }

  for(size_t i = 0; i < output_num(); i++) {
    size_t tmp;
    const std::string outname ("out" + std::to_string (i));
    output_port::sptr out = output(outname);
    msg::sptr m = new_msg(out);
    memcpy(m->raw_ptr(), &cnt, sizeof(size_t));
    write(out, m);
  }
}

}  // namespace testing

}  // namespace eqnx
