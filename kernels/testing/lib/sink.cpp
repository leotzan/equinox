/*
 * Equinox: SDR platform for realtime applications
 * Copyright (C) 2017  Manolis Surligas <surligas@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <equinox/kernels/testing/sink.h>

#include <cstring>

namespace eqnx
{

namespace testing
{

sink::sptr
sink::make_shared (const std::string& name, size_t nports)
{
  return std::shared_ptr<sink> (new sink (name, nports));
}

sink::sink (const std::string& name, size_t nports) :
        kernel (name)
{
  for (size_t i = 0; i < nports; i++) {
    const std::string pname ("in" + std::to_string (i));
    new_input_port (pname);
    d_stats.push_back(0);
  }
}

sink::~sink()
{
  size_t id = 0;
  for(input_port::sptr i : inputs()) {
    std::cout << "Port " << i->name() << " received " << d_stats[id] << std::endl;
    id++;
  }
}

void
sink::exec ()
{
  for(size_t i = 0; i < input_num(); i++) {
    size_t cnt;
    const std::string pname ("in" + std::to_string (i));
    input_port::sptr p = input(pname);
    msg::sptr m = p->read();
    memcpy(&cnt, m->raw_ptr(), sizeof(size_t));
    d_stats[i]++;
  }
}

}  // namespace testing

}  // namespace eqnx

